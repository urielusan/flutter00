//FlutterPackages----------------------------------------------------
import 'package:flutter/material.dart';

class MyTheme {
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //+ BaseColors                                                    +
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  static const Color baseColorA = Color.fromARGB(255, 200, 120, 255);
  static const Color baseColorB = Color.fromARGB(255, 41, 21, 49);
  static const Color baseColorC = Color.fromARGB(255, 255, 200, 255);
  static const Color baseColorD = Color.fromARGB(255, 122, 27, 185);

  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //+ MainTheme                                                     +
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  static final ThemeData myTheme = ThemeData(
    //Base-----------------------------------------------------------
    brightness: Brightness.dark,
    primaryColor: Colors.purpleAccent,
    //AppBar---------------------------------------------------------
    appBarTheme: const AppBarTheme(
      backgroundColor: baseColorB,
      elevation: 10.5,
    ),
    //Scaffold-------------------------------------------------------
    scaffoldBackgroundColor: baseColorA,
    //Icons
    iconTheme: const IconThemeData(
      color: baseColorC,
    ),
    //TextButton-----------------------------------------------------
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(primary: baseColorC),
    ),
    //FloatingActionButton-------------------------------------------
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
      backgroundColor: baseColorB,
      elevation: 7,
    ),
    //ElevatedButton-------------------------------------------------
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: baseColorB,
        shape: const ContinuousRectangleBorder(),
        elevation: 9,
      ),
    ),
    //TextFormField--------------------------------------------------
    inputDecorationTheme: InputDecorationTheme(
      floatingLabelStyle: const TextStyle(color: baseColorD),
      //Borde[Disponible]............................................
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: baseColorB.withOpacity(0.5)),
        borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      //Borde[Activo]................................................
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: baseColorB.withOpacity(0.75)),
        borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      //Borde[Base?].................................................
      border: OutlineInputBorder(
        borderSide: BorderSide(color: baseColorB.withOpacity(0.25)),
        borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
    ),
    //---------------------------------------------------------------
  );
}
