import 'package:flutter/material.dart';
import 'package:myapp00/themes/my_theme.dart';

class MyCard01 extends StatelessWidget {
  //Ul campo es obligatorio
  final String imageURL;
  //El campo es opcional
  final String? name;

  const MyCard01({
    Key? key,
    required this.imageURL,
    this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.antiAlias,
      elevation: 50,
      shadowColor: MyTheme.baseColorB.withOpacity(0.7),
      //shape: const RoundedRectangleBorder(
      //  borderRadius: BorderRadius.zero,
      //),
      //.............................................................
      child: Column(
        children: [
          //Primer tipo de imagen....................................
          /*Image(
            image: NetworkImage(
                'https://files.yande.re/image/9f58aa98bd3167c854430bf99d9027b5/yande.re%20751163%20akemi_homura%20dress%20heels%20nardack%20no_bra%20puella_magi_madoka_magica%20skirt_lift%20thighhighs%20ultimate_madoka%20wings.jpg'),
          ),*/
          //Segundo tipo de imagen...................................
          FadeInImage(
            placeholder: const AssetImage('assets/images/pikachu.gif'),
            image: NetworkImage(imageURL),
            width: double.infinity,
            height: 550,
            fit: BoxFit.cover,
            fadeInDuration: const Duration(milliseconds: 500),
          ),

          if (name != null)
            Container(
              alignment: AlignmentDirectional.centerEnd,
              padding: const EdgeInsets.only(top: 10, bottom: 10, right: 20),
              //En caso de que no se asigne un nombre, muestra el siguiente texto
              //Aunque ya se anda checando con la condición
              child: Text(name ?? '-XxxxX-'),
            ),
        ],
      ),
    );
  }
}
