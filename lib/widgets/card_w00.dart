import 'package:flutter/material.dart';

class MyCard00 extends StatelessWidget {
  const MyCard00({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          const ListTile(
            leading: Icon(Icons.photo_album_outlined),
            title: Text('Experiment [暁美 ほむら]'),
            subtitle: Text('Don´t forget.\n'
                'Always, somewhere,\n'
                'Someone is fighting for you.\n'
                'As long as you remember her,\n'
                'you are not alone.'),
          ),
          //Filas para los botones...................................
          Padding(
            padding: const EdgeInsets.only(top: 3),
            //Se encerro en un padding [control + '.']
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(onPressed: () {}, child: const Text('はい')),
                TextButton(onPressed: () {}, child: const Text('いいえ')),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
