import 'package:flutter/material.dart';

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+ MyTextFormField                                                 +
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//TextFormField[Final]-----------------------------------------------
class MyTextField extends StatelessWidget {
  //Data.............................................................
  final String formProperty;
  final Map<String, String> dataMap;
  //Textos...........................................................
  final String? hintTxt;
  final String? labelTxt;
  final String? helperTxt;
  final String? counterTxt;
  //Iconos...........................................................
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  //Other............................................................
  final bool eMail;
  final bool hideTxt;

  const MyTextField({
    Key? key,
    required this.formProperty,
    required this.dataMap,
    this.hintTxt,
    this.labelTxt,
    this.helperTxt,
    this.counterTxt,
    this.prefixIcon,
    this.suffixIcon,
    this.eMail = false,
    this.hideTxt = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      //Activa el teclado luego luego en este campo
      autofocus: false,
      //focusNode: FocusNode(),

      initialValue: '',
      textCapitalization: TextCapitalization.words,
      //Configuracion extra------------------------------------------
      keyboardType: eMail ? TextInputType.emailAddress : null,
      obscureText: hideTxt,
      //Eventos------------------------------------------------------
      onChanged: (value) => dataMap[formProperty] = value,

      validator: (value) {
        if (value!.isEmpty) {
          return 'Campo vacio...';
        }
        //if (value == null) return 'Campo vacio';
        return value.length < 3 ? 'Colocar un minimo de 3 caracteres' : null;
      },

      autovalidateMode: AutovalidateMode.onUserInteraction,
      //Decoracion---------------------------------------------------
      decoration: InputDecoration(
        hintText: hintTxt,
        labelText: labelTxt,
        helperText: helperTxt,
        counterText: counterTxt,
        //Iconos.....................................................
        icon: const Icon(
          Icons.ac_unit,
          color: Color.fromARGB(255, 100, 220, 255),
        ),
        prefixIcon: prefixIcon == null ? null : Icon(prefixIcon),
        suffixIcon: suffixIcon == null ? null : Icon(suffixIcon),
      ),
      //.............................................................
    );
  }
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+ MyTextFormField                                                 +
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//TextFormField de prueba--------------------------------------------
/*
class MyTestTextField extends StatelessWidget {
  const MyTestTextField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      //Activa el teclado luego luego en este campo
      //autofocus: true,
      initialValue: '',
      textCapitalization: TextCapitalization.words,
      onChanged: (value) {
        //...
      },
      //Validación---------------------------------------------------
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (value) {
        if (value!.isEmpty) {
          return 'Campo vacio...';
        }
        return value.length < 3 ? 'Colocar un minimo de 3 caracteres' : null;
      },
      //Decoracion---------------------------------------------------
      decoration: const InputDecoration(
        hintText: 'Nick',
        labelText: 'Nickname:',
        helperText: 'Miau!!! =OwO=',
        counterText: '[000]',
        //Iconos.....................................................
        icon: Icon(
          Icons.ac_unit,
          color: Color.fromARGB(255, 100, 220, 255),
        ),
        prefixIcon: Icon(Icons.account_circle),
        suffixIcon: Icon(Icons.check_box_outline_blank_rounded),
      ),
      //.............................................................
    );
  }
}
*/