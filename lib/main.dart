//FlutterPackages----------------------------------------------------
import 'package:flutter/material.dart';
//ThirdPackages------------------------------------------------------
//CustomPackages-----------------------------------------------------
import 'package:myapp00/router/app_router.dart';
import 'package:myapp00/themes/my_theme.dart';

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+ Main funtion                                                    +
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void main() {
  //Indico el widget que contiene la app
  runApp(const MyApp());
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+ Widgets                                                         +
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Widget contenedor de la app----------------------------------------
class MyApp extends StatelessWidget {
  //VS pide generar este constructor.................................
  const MyApp({Key? key}) : super(key: key);

  //Método build necesario para el widget............................
  @override
  Widget build(BuildContext context) {
    //Material regresado con propiedades del widget
    return MaterialApp(
      //Quitar la etiqueta de debug
      debugShowCheckedModeBanner: false,
      title: 'Material App',

      //Tema asignado al widget
      theme: MyTheme.myTheme,

      //Llamar pantalla
      initialRoute: AppRouter.initialRoute,
      routes: AppRouter.getMainMenuRoutes(),
    );
  }
}

//-------------------------------------------------------------------
//Widget anterior----------------------------------------------------
/*class MyAppX extends StatelessWidget {
  //VS pide generar este constructor.................................
  const MyAppX({Key? key}) : super(key: key);

  //Método build necesario para el widget............................
  @override
  Widget build(BuildContext context) {
    //Material regresado con propiedades del widget
    return MaterialApp(
      //Quitar la etiqueta de debug
      debugShowCheckedModeBanner: false,
      //Tema asignado al widget
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.purpleAccent,
        fontFamily: 'Arial',
        backgroundColor: const Color.fromARGB(255, 200, 100, 255),
      ),
      //Imprime texto centrado
      home: const HomeTest(),
    );
  }
}*/

//-------------------------------------------------------------------
//Notas--------------------------------------------------------------
//-Regularmente cuando algo no cambia VS te recomienda hacer algo
// como "const".
//-Tener cuidado con las constantes, se puede reorganizar hijos y
// propiedades.
