//FlutterPackages----------------------------------------------------
import 'package:flutter/material.dart';
//CustomPackages-----------------------------------------------------
import 'package:myapp00/models/models.dart';
import 'package:myapp00/screens/screens.dart';

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+ RouterClass                                                     +
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class AppRouter {
  static const String initialRoute = 'home';

  static final menuOptions = <MenuOptions>[
    MenuOptions(
        route: 'home',
        name: 'HomeTest',
        screen: const HomeTest(),
        icon: Icons.home_outlined),
    /*MenuOptions(
        route: 'homeList',
        name: 'HomeList',
        screen: HomeList(),
        icon: Icons.add_circle_outline),*/
    MenuOptions(
        route: 'avatar',
        name: 'Avatar',
        screen: const Avatar(),
        icon: Icons.portrait_rounded),
    MenuOptions(
        route: 'tarjeta',
        name: 'Tarjetas',
        screen: const Tarjeta(),
        icon: Icons.photo_album_outlined),
    MenuOptions(
        route: 'scrolling_img',
        name: 'Scrolling',
        screen: const ScrollingImages(),
        icon: Icons.satellite_outlined),
    MenuOptions(
        route: 'slider_img',
        name: 'Slider',
        screen: const ImageSlider(),
        icon: Icons.zoom_in_map_rounded),
    MenuOptions(
        route: 'animacion',
        name: 'Animacion',
        screen: const AnimationScreen(),
        icon: Icons.movie_creation_outlined),
    MenuOptions(
        route: 'contador',
        name: 'Contador',
        screen: const CounterScreen(),
        icon: Icons.add_circle_outline),
    MenuOptions(
        route: 'formulario',
        name: 'Formulario',
        screen: Formulario(),
        icon: Icons.format_list_bulleted_sharp),
    MenuOptions(
        route: 'calculadora',
        name: 'Calculadora',
        screen: const Calculadora(),
        icon: Icons.calculate_outlined),
    MenuOptions(
        route: 'alerta',
        name: 'Alerta',
        screen: const Alerta(),
        icon: Icons.warning_amber_rounded),
  ];

  static Map<String, Widget Function(BuildContext)> getMainMenuRoutes() {
    Map<String, Widget Function(BuildContext)> menuRoutes = {};

    //Por si quiero quitar el 'Home' de la lista
    //menuRoutes.addAll({'home': (BuildContext context) => const HomeTest()});

    for (final option in menuOptions) {
      menuRoutes.addAll({
        option.route: (BuildContext context) => option.screen,
      });
    }

    return menuRoutes;
  }

  /*static Map<String, Widget Function(BuildContext)> routes =
      <String, WidgetBuilder>{
    'home': (BuildContext context) => const HomeTest(),
    'homeList': (BuildContext context) => HomeList(),
    'contador': (BuildContext context) => const CounterScreen(),
    'calculadora': (BuildContext context) => const Calculadora(),
  };*/

  /*static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) => const Calculadora(),
    );
  }*/
}
