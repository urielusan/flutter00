import 'package:flutter/material.dart';
import 'package:myapp00/themes/my_theme.dart';

import 'dart:math';

class AnimationScreen extends StatefulWidget {
  const AnimationScreen({Key? key}) : super(key: key);

  @override
  State<AnimationScreen> createState() => _AnimationScreenState();
}

class _AnimationScreenState extends State<AnimationScreen> {
  final random = Random();

  bool state = true;
  double _width = 200;
  double _height = 200;
  Color _color = Colors.black;
  BorderRadiusGeometry _borderRadius = BorderRadius.circular(5);

  void changeShape() {
    if (state) {
      _width = random.nextInt(300).toDouble() + 10;
      _height = random.nextInt(300).toDouble() + 10;
      _color = Color.fromRGBO(
        random.nextInt(255),
        random.nextInt(255),
        random.nextInt(255),
        1,
        //random.nextDouble(),
      );
      _borderRadius = BorderRadius.circular(random.nextInt(100).toDouble() + 1);
    } else {
      _width = 200;
      _height = 200;
      _color = Colors.black;
      _borderRadius = BorderRadius.circular(50);
    }
    state = !state;

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        //Contenedor base
        //child: Container(
        //Contenedor animado
        child: AnimatedContainer(
          //Propiedades[AnimatedContainer]
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeInExpo,
          //Propiedades[General]
          width: _width,
          height: _height,
          decoration: BoxDecoration(
            color: _color,
            borderRadius: _borderRadius,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(
          Icons.play_circle_outline,
          size: 50,
          color: MyTheme.baseColorC,
        ),
        onPressed: () => changeShape(),
      ),
    );
  }
}

//Notas:
//-Dentro de un container no puedes tener a la vez:
//--BoxDecoration
//--Color