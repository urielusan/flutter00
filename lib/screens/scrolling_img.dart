import 'package:flutter/material.dart';
import 'package:myapp00/themes/my_theme.dart';

class ScrollingImages extends StatefulWidget {
  const ScrollingImages({Key? key}) : super(key: key);

  @override
  State<ScrollingImages> createState() => _ScrollingImagesState();
}

class _ScrollingImagesState extends State<ScrollingImages> {
  final List<int> imagesId = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  final ScrollController scrollV = ScrollController();

  bool isLoading = false;

  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //+ Metodos                                                                 +
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @override
  void initState() {
    super.initState();

    scrollV.addListener(() {
      //print('ActualPixelPosition: ${scrollV.position.pixels}\n'
      //    'MaxSize: ${scrollV.position.maxScrollExtent}\n');

      //El valore de la suma parece exagerarse para "disminuir el tiempo de carga",
      //debido a que al estar mas arriba permite un ligero tiempo extra de carga
      if ((scrollV.position.pixels + 10) >= scrollV.position.maxScrollExtent) {
        fetchData();
      }
    });
  }

  //Método asyncrono-----------------------------------------------------------
  Future fetchData() async {
    if (isLoading) return;

    isLoading = true;
    setState(() {});

    await Future.delayed(const Duration(milliseconds: 1250));

    add5toArray();
    isLoading = false;
    setState(() {});

    //Evitar la animación si estas arriba de la pantalla
    if (scrollV.position.pixels + 30 <= scrollV.position.maxScrollExtent) {
      return;
    }

    scrollV.animateTo(
      scrollV.position.pixels + 200,
      duration: const Duration(milliseconds: 750),
      curve: Curves.decelerate,
    );
  }

  //Función para el refresh----------------------------------------------------
  Future<void> onRefresh() async {
    await Future.delayed(const Duration(seconds: 2));
    final lastId = imagesId.last;
    imagesId.clear();
    imagesId.add(lastId + 1);
    add5toArray();
    //Equivale a
    //return null;
  }

  //Añadir al arreglo----------------------------------------------------------
  void add5toArray() {
    final lastId = imagesId.last;
    imagesId.addAll([1, 2, 3, 4, 5].map((e) => lastId + e));

    //No te olvides de este metodo para el refresh
    setState(() {});
  }

  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  //+ Widget                                                                +
  //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: MediaQuery.removePadding(
        context: context,
        removeTop: true,
        removeBottom: true,
        child: Stack(
          children: [
            //Refresh..........................................................
            RefreshIndicator(
              onRefresh: onRefresh,
              color: MyTheme.baseColorC,
              backgroundColor: MyTheme.baseColorA.withOpacity(0.5),
              strokeWidth: 4,
              //ListView.......................................................
              child: ListView.builder(
                physics: const BouncingScrollPhysics(),
                controller: scrollV,
                itemCount: imagesId.length,
                itemBuilder: (BuildContext context, int index) {
                  return FadeInImage(
                    width: double.infinity,
                    height: 300,
                    fit: BoxFit.cover,
                    placeholder: const AssetImage('assets/images/pikachu.gif'),
                    image: NetworkImage(
                        'https://picsum.photos/500/300?image=${index + 1}'),
                  );
                },
              ),
            ),
            //Container........................................................
            //Solo puedes tener una instrucción por condicional
            if (isLoading)
              Positioned(
                bottom: 40,
                left: size.width * 0.5 - 50,
                child: const LoadingContainer(),
              ),
          ],
        ),
      ),
    );
  }
}

class LoadingContainer extends StatelessWidget {
  const LoadingContainer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(30),
      height: 100,
      width: 100,
      decoration: BoxDecoration(
        color: MyTheme.baseColorA.withOpacity(0.5),
        shape: BoxShape.circle,
      ),
      child: const CircularProgressIndicator(
        color: MyTheme.baseColorC,
        strokeWidth: 8,
      ),
    );
  }
}
