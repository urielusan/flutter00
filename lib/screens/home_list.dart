//FlutterPackages----------------------------------------------------
import 'package:flutter/material.dart';
//CustomPackages-----------------------------------------------------
import 'package:myapp00/router/app_router.dart';

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+ Widgets                                                         +
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//ListWidget---------------------------------------------------------
class HomeList extends StatelessWidget {
  final mainOptions = AppRouter.menuOptions;

  HomeList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      //Cantidad de elementos........................................
      itemCount: mainOptions.length,
      //Para modificar los elementos.................................
      itemBuilder: (context, i) => ListTile(
        title: Text(mainOptions[i].name),
        leading: Icon(mainOptions[i].icon),
        trailing: const Icon(Icons.arrow_forward_ios_outlined),
        //Accion al presionar........................................
        onTap: () {
          if (mainOptions[i].route != 'home') {
            Navigator.pushNamed(context, mainOptions[i].route);
          }
        },
      ),
      //Cuando una variable no se emplea se suele usar '_'
      separatorBuilder: (_, __) => const Divider(),
    );
  }
}
