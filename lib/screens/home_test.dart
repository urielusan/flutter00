//FlutterPackages----------------------------------------------------
import 'package:flutter/material.dart';
//CustomPackages-----------------------------------------------------
import 'package:myapp00/screens/screens.dart';

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+ Widgets                                                         +
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class HomeTest extends StatelessWidget {
  //Constructor......................................................
  //El key sirve para identificar el widget
  const HomeTest({Key? key}) : super(key: key);

  //Build............................................................
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text('-XxxxX-'),
        ),
      ),
      body: Center(
        child: HomeList(),
      ),
    );
  }
}
