import 'package:flutter/material.dart';
import 'package:myapp00/themes/my_theme.dart';

class ImageSlider extends StatefulWidget {
  const ImageSlider({Key? key}) : super(key: key);

  @override
  State<ImageSlider> createState() => _ImageSliderState();
}

class _ImageSliderState extends State<ImageSlider> {
  double _sliderValue = 100;
  bool _sliderOn = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Slider')),
      body: Column(
        children: [
          //Deslizador...............................................
          Slider(
            value: _sliderValue,
            min: 0,
            max: 500,
            activeColor: MyTheme.baseColorB,
            onChanged: _sliderOn
                ? (value) {
                    _sliderValue = value;
                    setState(() {});
                  }
                : null,
          ),
          //Checks...................................................
          CheckboxListTile(
            title: const Text('[Checkbox]-Slider'),
            activeColor: MyTheme.baseColorB,
            value: _sliderOn,
            onChanged: (value) => setState(() {
              _sliderOn = value ?? true;
            }),
          ),

          SwitchListTile(
            title: const Text('[Switch]-Slider'),
            activeColor: MyTheme.baseColorB,
            value: _sliderOn,
            onChanged: (value) => setState(() {
              _sliderOn = value;
            }),
          ),
          //Imagen...................................................
          Image(
            image: const NetworkImage(
                'https://files.yande.re/image/15af03a2bdd54895faec1192510527ad/yande.re%20165389%20gokou_ruri%20gothic_lolita%20lolita_fashion%20ore_no_imouto_ga_konnani_kawaii_wake_ga_nai%20yukian.jpg'),
            fit: BoxFit.contain,
            width: _sliderValue,
          ),

          //No busco este espacio al final
          //const SizedBox(height: 50),
          //const AboutListTile(),
        ],
      ),
    );
  }
}
