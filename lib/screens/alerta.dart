//FlutterPackages----------------------------------------------------
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//ThirdPackages------------------------------------------------------
import 'dart:io';
//CustomPackages-----------------------------------------------------
import 'package:myapp00/themes/my_theme.dart';

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+ Widget[Alerta]                                                  +
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class Alerta extends StatelessWidget {
  const Alerta({Key? key}) : super(key: key);

  //Alert[Iphone]----------------------------------------------------
  void showIphoneAlert(BuildContext ctx) {
    showCupertinoDialog(
      context: ctx,
      builder: (ctx) {
        return CupertinoAlertDialog(
          title: const Text('Miau =OwO=!!!'),
          content: const AlertContent(),
          actions: [
            TextButton(
              child: const Text(
                'Regresar',
                style: TextStyle(fontSize: 15),
              ),
              onPressed: () => Navigator.pop(ctx),
            ),
            TextButton(
              child: Column(
                children: const [
                  Text(
                    'Lo mismo',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: MyTheme.baseColorC, fontSize: 15),
                  ),
                  Text(
                    '[Pero en otro color]',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: MyTheme.baseColorA, fontSize: 8),
                  ),
                ],
              ),
              onPressed: () => Navigator.pop(ctx),
            ),
          ],
        );
      },
    );
  }

  //Alert[Android]---------------------------------------------------
  void showAndroidAlert(BuildContext ctx) {
    showDialog(
        barrierDismissible: true,
        context: ctx,
        builder: (ctx) {
          //Dialog...................................................
          return AlertDialog(
            title: const Text('MesaggeAlert!!!'),
            elevation: 200,
            shape: const ContinuousRectangleBorder(
              borderRadius: BorderRadius.zero,
            ),
            content: const AlertContent(),
            actions: [
              TextButton(
                child: const Icon(Icons.close),
                onPressed: () => Navigator.pop(ctx),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //.............................................................
      body: Center(
        child: ElevatedButton(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 30,
              vertical: 10,
            ),
            child: Text(
              'TstAlert!!',
              style: TextStyle(
                  color: Colors.red.shade800,
                  fontSize: 40,
                  fontStyle: FontStyle.italic,
                  shadows: <Shadow>[
                    Shadow(
                      color: Colors.red.shade200,
                      offset: Offset.zero,
                      blurRadius: 20,
                    ),
                  ]),
            ),
          ),
          onPressed: () {
            //Me gusto mas el material -w-
            if (Platform.isIOS) {
              showIphoneAlert(context);
            } else {
              //showAndroidAlert(context);
              showIphoneAlert(context);
            }
          },
          style: ElevatedButton.styleFrom(
            primary: MyTheme.baseColorC,
          ),
        ),
      ),
      //.............................................................
      floatingActionButton: FloatingActionButton(
        child: const Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+ Widget[Alerta(Contenido)]                                       +
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
class AlertContent extends StatelessWidget {
  const AlertContent({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const Text(
          '⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿\n'
          '⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿\n'
          '⡟⢹⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡛⢿⣿⣿⣿⣮⠛⣿⣿⣿⣿\n'
          '⡇⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣣⠄⡀⢬⣭⣻⣷⡌⢿⣿⣿\n'
          '⡀⠈⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⠈⣆⢹⣿⣿⣿⡈⢿⣿\n'
          '⡇⠄⢛⣛⣻⣿⣿⣿⣿⣿⣿⣿⣿⡆⠹⣿⣆⠸⣆⠙⠛⠛⠃⠘⣿\n'
          '⣡⠄⡈⣿⣿⣿⣿⣿⣿⣿⣿⠿⠟⠁⣠⣉⣤⣴⣿⣿⠿⠿⠿⡇⢸\n'
          '⢿⣆⠰⡘⢿⣿⠿⢛⣉⣥⣴⣶⣿⣿⣿⣿⣻⠟⣉⣤⣶⣶⣾⣿⡄\n'
          '⠸⣿⠄⢳⣠⣤⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⣼⣿⣿⣿⣿⣿⣿⡇\n'
          '⢣⣡⣶⠿⠟⠛⠓⣚⣻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣇\n'
          '⠄⠻⣧⣴⣾⣿⣿⣿⣿⣿⣿⣿⣿⡿⠟⠛⠛⠛⢿⣿⣿⣿⣿⣿⡟\n'
          '⣷⡀⠘⣿⣿⣿⣿⣿⣿⣿⣿⡋⢀⣠⣤⣶⣶⣾⡆⣿⣿⣿⠟⠁⠄\n'
          '⡘⣿⡀⢻⣿⣿⣿⣿⣿⣿⣿⣧⠸⣿⣿⣿⣿⣿⣷⡿⠟⠉⠄⠄⠄\n'
          '⣷⡈⢷⡀⠙⠛⠻⠿⠿⠿⠿⠿⠷⠾⠿⠟⣛⣋⣥⣶⣄⠄⢀⣄⠹\n',
        ),
        const SizedBox(height: 5),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const [
            FlutterLogo(size: 30),
            FlutterLogo(size: 30),
            FlutterLogo(size: 30),
            FlutterLogo(size: 30),
            FlutterLogo(size: 30),
          ],
        ),
      ],
    );
  }
}
