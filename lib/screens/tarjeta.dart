import 'package:flutter/material.dart';
import 'package:myapp00/widgets/widgets.dart';

class Tarjeta extends StatelessWidget {
  const Tarjeta({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: const Text('MyCards'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 30),
        children: const [
          //Tarjeta[00]
          MyCard00(),
          //El size box es para rellenar espacio
          SizedBox(height: 3),
          //Tarjeta[01]
          MyCard01(
            name: 'Akemi[00]',
            imageURL:
                'https://files.yande.re/image/9f58aa98bd3167c854430bf99d9027b5/yande.re%20751163%20akemi_homura%20dress%20heels%20nardack%20no_bra%20puella_magi_madoka_magica%20skirt_lift%20thighhighs%20ultimate_madoka%20wings.jpg',
          ),
          MyCard01(
            //name: 'Akemi[01]',
            imageURL:
                'https://files.yande.re/jpeg/f7d1d57c1a872391b87ea5b2e79c2da9/yande.re%20520432%20akemi_homura%20dress%20fishnets%20kyubey%20puella_magi_madoka_magica%20skirt_lift%20stockings%20thighhighs%20water0889.jpg',
          ),
          MyCard01(
            name: 'Akemi[02]',
            imageURL:
                'https://files.yande.re/image/74468351ff7ba0045e2de458ca24d4df/yande.re%20444802%20akemi_homura%20cleavage%20dress%20heels%20jiinyo_%28awamoe1207%29%20puella_magi_madoka_magica%20thighhighs%20wings.jpg',
          ),
          MyCard01(
            //name: 'Akemi[03]',
            imageURL:
                'https://files.yande.re/image/13ee9ec1d2b20d903c7035388b558bcf/yande.re%20441508%20akemi_homura%20cleavage%20puella_magi_madoka_magica%20thighhighs%20wings.jpg',
          ),
          MyCard01(
            name: 'Akemi[04]',
            imageURL:
                'https://files.yande.re/image/ca43ae8fe97d344286007b65209e0d77/yande.re%20441511%20akemi_homura%20heels%20jiinyo_%28awamoe1207%29%20pantyhose%20puella_magi_madoka_magica.jpg',
          ),
          MyCard01(
            imageURL:
                'https://files.yande.re/image/5476139d1a1d1f99c2cba36ae70f3695/yande.re%20434440%20akemi_homura%20kaname_madoka%20puella_magi_madoka_magica%20seifuku%20valentine%20yamada_ako%20yuri.jpg',
          ),
        ],
      ),
    );
  }
}
