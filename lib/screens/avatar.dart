import 'package:flutter/material.dart';
import 'package:myapp00/themes/my_theme.dart';

class Avatar extends StatelessWidget {
  const Avatar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
        actions: [
          Container(
            margin: const EdgeInsets.all(10),
            child: CircleAvatar(
              child:
                  const Text('XY', style: TextStyle(color: MyTheme.baseColorC)),
              backgroundColor: MyTheme.baseColorA.withOpacity(0.25),
            ),
          ),
        ],
      ),
      body: const Center(
        child: CircleAvatar(
          maxRadius: 200,
          backgroundImage: NetworkImage(
              'https://files.yande.re/image/544b306ef3836dad913f10ff1e0e2544/yande.re%20263305%20rozen_maiden%20suigintou%20wings%20yasuyuki.jpg'),
        ),
      ),
    );
  }
}
