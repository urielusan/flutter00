import 'package:flutter/material.dart';

class CounterScreen extends StatefulWidget {
  const CounterScreen({Key? key}) : super(key: key);

  @override
  State<CounterScreen> createState() => _CounterScreenState();
}

class _CounterScreenState extends State<CounterScreen> {
  int counter = 0;

  void increase() {
    counter++;
    setState(() {});
  }

  void decrease() {
    counter--;
    setState(() {});
  }

  void reset() {
    counter = 0;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    const fontSize30 = TextStyle(fontSize: 30);

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('TouchTimes', style: fontSize30),
            Text('$counter', style: fontSize30),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: CustomFloatingActions(
        increaseFn: increase,
        decreaseFn: decrease,
        resetFn: reset,
      ),
    );
  }
}

class CustomFloatingActions extends StatelessWidget {
  final Function increaseFn;
  final Function decreaseFn;
  final Function resetFn;

  const CustomFloatingActions({
    Key? key,
    required this.increaseFn,
    required this.decreaseFn,
    required this.resetFn,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        FloatingActionButton(
          heroTag: '-',
          child: const Icon(
            Icons.exposure_minus_1_outlined,
            color: Colors.white,
          ),
          onPressed: () => decreaseFn(),
        ),
        FloatingActionButton(
          heroTag: '0',
          child: const Icon(
            Icons.exposure_outlined,
            color: Colors.white,
          ),
          onPressed: () {
            resetFn();
          },
        ),
        FloatingActionButton(
          heroTag: '+',
          child: const Icon(
            Icons.exposure_plus_1_outlined,
            color: Colors.white,
          ),
          onPressed: () => increaseFn(),
        ),
      ],
    );
  }
}

//Notas:
//-El contador es copiado para ver como es el traspaso de funciones entre hijos