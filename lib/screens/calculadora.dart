//FlutterPackages----------------------------------------------------
import 'package:flutter/material.dart';
//CustomPackages-----------------------------------------------------
import 'package:myapp00/logic/logic_calculadora.dart';
import 'package:myapp00/themes/my_theme.dart';

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+ Widgets                                                         +
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Calculadora[Container]---------------------------------------------
class Calculadora extends StatelessWidget {
  const Calculadora({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: const <Widget>[
          CalculadoraDisplay(),
          Center(
            child: CalculadoraButtons(),
          ),
        ],
      ),
    ));
  }
}

//-------------------------------------------------------------------
//Calculadora[Display]-----------------------------------------------
class CalculadoraDisplay extends StatefulWidget {
  const CalculadoraDisplay({Key? key}) : super(key: key);

  @override
  State<CalculadoraDisplay> createState() => _CalculadoraDisplayState();
}

class _CalculadoraDisplayState extends State<CalculadoraDisplay> {
  String operation = LogicCalculadora.operation;

  void operationUpdate() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    //Definir constante para la fuente
    const fStyle = TextStyle(fontSize: 40, shadows: <Shadow>[
      Shadow(
        offset: Offset(10, 10),
        blurRadius: 0,
        color: Color.fromARGB(94, 90, 0, 163),
      ),
      Shadow(
        offset: Offset(20, 20),
        blurRadius: 5,
        color: Color.fromARGB(61, 118, 0, 253),
      ),
    ]);

    //Contenedor
    return Container(
      alignment: Alignment.center,
      constraints: const BoxConstraints.expand(
        height: 100,
      ),
      color: MyTheme.baseColorB,
      child: Text(
        LogicCalculadora.operation,
        style: fStyle,
      ),
    );
  }
}

//-------------------------------------------------------------------
//Calculadora[Buttons]-----------------------------------------------
class CalculadoraButtons extends StatelessWidget {
  const CalculadoraButtons({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        //Columna[00]................................................
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            //FirstTry
            //Text('+'),
            //Text('-'),
            //Text('*'),
            //Text('/'),

            IndividualButton(txt: '+'),
            IndividualButton(txt: '-'),
            IndividualButton(txt: '*'),
            IndividualButton(txt: '/'),
          ],
        ),
        //Columna[01]................................................
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            IndividualButton(txt: '('),
            IndividualButton(txt: '1'),
            IndividualButton(txt: '2'),
            IndividualButton(txt: '3'),
          ],
        ),
        //Columna[02]................................................
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            IndividualButton(txt: ')'),
            IndividualButton(txt: '4'),
            IndividualButton(txt: '5'),
            IndividualButton(txt: '6'),
          ],
        ),
        //Columna[03]................................................
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            IndividualButton(txt: 'E'),
            IndividualButton(txt: '7'),
            IndividualButton(txt: '8'),
            IndividualButton(txt: '9'),
          ],
        ),
        //Columna[04]................................................
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[
            IndividualButton(txt: '='),
            IndividualButton(txt: 'R'),
            IndividualButton(txt: '0'),
            IndividualButton(txt: '.'),
          ],
        ),
      ],
    );
  }
}

//-------------------------------------------------------------------
//Button[Widget]-----------------------------------------------------
class IndividualButton extends StatelessWidget {
  final String txt;

  const IndividualButton({
    Key? key,
    required this.txt,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      //Texto
      child: Text(txt),
      //Acción
      onPressed: () {
        LogicCalculadora.addChar(txt);
      },
    );
  }
}

//Notas:
//-Checar curso 57 para las funciones