import 'package:flutter/material.dart';
import 'package:myapp00/widgets/widgets.dart';

class Formulario extends StatelessWidget {
  //En este caso se utilizara el key del widget
  final GlobalKey<FormState> myFormKey = GlobalKey<FormState>();

  Formulario({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //En este caso se utilizara el key del widget
    //final GlobalKey<FormState> myFormKey = GlobalKey<FormState>();

    //Mapa que almacenara los valores del formulario
    final Map<String, String> formDataV = {
      'nickProp': 'a',
      'mailProp': '@',
      'passProp': '.',
      //Semi harcodeado ya que si no se selecciona algo en el
      //"dropdown" no hay modificación alguna
      'roleProp': 'Type -A-',
    };

    return Scaffold(
      appBar: AppBar(
        title: const Text('Formulario'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 35),
          //Formulario+++++++++++++++++++++++++++++++++++++++++++++++
          child: Form(
            //Asigno la llave----------------------------------------
            key: myFormKey,
            //autovalidateMode: AutovalidateMode.onUserInteraction,
            //Columnas-----------------------------------------------
            child: Column(
              children: [
                //TextForm...........................................
                MyTextField(
                  formProperty: 'nickProp',
                  dataMap: formDataV,
                  labelTxt: 'Nickname',
                  hintTxt: 'nick',
                ),
                const SizedBox(height: 10),
                //...................................................
                MyTextField(
                  formProperty: 'mailProp',
                  dataMap: formDataV,
                  labelTxt: 'Mail',
                  hintTxt: 'e-mail',
                  eMail: true,
                ),
                const SizedBox(height: 10),
                //...................................................
                MyTextField(
                  formProperty: 'passProp',
                  dataMap: formDataV,
                  labelTxt: 'Password',
                  hintTxt: 'pass',
                  hideTxt: true,
                ),
                const SizedBox(height: 15),
                //...................................................
                DropdownButtonFormField<String>(
                  //Debe de tomar algun valor de bajo
                  value: 'TestA',

                  //Objetos de la lista
                  items: const [
                    DropdownMenuItem(value: 'TestA', child: Text('Type -A-')),
                    DropdownMenuItem(value: 'TestB', child: Text('Type -B-')),
                    DropdownMenuItem(value: 'TestC', child: Text('Type -C-')),
                  ],
                  //Evento al cambiar
                  onChanged: (value) {
                    //'??' En caso de ser null
                    formDataV['roleProp'] = value ?? 'Type -Null-';
                  },
                ),
                const SizedBox(height: 15),
                //...................................................
                ElevatedButton(
                  child: const SizedBox(
                    child: Center(
                      child: Text(
                        'Confirm',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    width: double.infinity,
                    height: 50,
                  ),
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                  onPressed: () {
                    //Esto es para desactivar el teclado
                    //Sería bueno para limpiar todos los campos
                    ///Me genera errores
                    //FocusScope.of(context).requestFocus(FocusNode());

                    if (!myFormKey.currentState!.validate()) {
                      print('Formulario no valido');
                      return;
                    }

                    print(formDataV);
                  },
                ),
                //+++++++++++++++++++++++++++++++++++++++++++++++++++++
              ],
            ),
          ),
        ),
      ),
    );
  }
}
